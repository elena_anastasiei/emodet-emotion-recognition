#include <com_example_ele_ndkopencvtest1_OepncvClass.h>

String face_cascade_name = "/storage/emulated/0/data/haarcascade_frontalface_default.xml";
CascadeClassifier face_cascade;
std::vector<Rect> faces;
Mat frame_gray;

JNIEXPORT void JNICALL Java_com_example_ele_ndkopencvtest1_OepncvClass_faceDetection
  (JNIEnv *env, jclass obj, jlong addrRgba){
    Mat& frame = *(Mat*)addrRgba;
    detect(frame);
  }

void detect(Mat& frame){
    if (face_cascade.empty()){
        if(!face_cascade.load(face_cascade_name)){
           printf("--(!)Error loading\n");
        }
     }

    cvtColor( frame, frame_gray, CV_BGR2GRAY );
    //const int scale = 3;
    //cv::Mat resized_frame_gray( cvRound( frame_gray.rows / scale ), cvRound( frame_gray.cols / scale ), CV_8UC1 );
    //cv::resize( frame_gray, resized_frame_gray, resized_frame_gray.size() );
    equalizeHist( frame_gray, frame_gray );

    //-- Detect faces
    face_cascade.detectMultiScale( frame_gray, faces, 1.3, 7, 0|CV_HAAR_SCALE_IMAGE, Size(48, 48) );

    for( size_t i = 0; i < faces.size(); i++ )
        {
        //create a Rect with top-left vertex at (x,y), of width and height pixels.
        Rect r = Rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
        rectangle(frame, r , Scalar( 255, 0, 0 ), 2, 8, 0 );
        //draw the rect defined by r with line thickness 2 and Blue color

        //Mat faceROI = frame_gray( faces[i] );
        }

}