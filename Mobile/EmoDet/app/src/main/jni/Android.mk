LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

OPENCVROOT:= C:\opencv-3.4.1-android-sdk
OPENCV_CAMERA_MODULES:=on
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=SHARED

include ${OPENCVROOT}/sdk/native/jni/OpenCV.mk

LOCAL_SRC_FILES := com_example_ele_ndkopencvtest1_OepncvClass.cpp

LOCAL_LDLIBS += -llog
LOCAL_MODULE := MyOpenCvLib


include $(BUILD_SHARED_LIBRARY)