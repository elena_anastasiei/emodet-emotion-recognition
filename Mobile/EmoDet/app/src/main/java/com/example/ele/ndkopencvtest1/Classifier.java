package com.example.ele.ndkopencvtest1;

public interface Classifier {
    String name();

    Classification recognize(final float[] pixels);
}
