package com.example.ele.ndkopencvtest1;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import org.opencv.android.*;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import android.graphics.Bitmap;

import java.lang.Thread;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import android.view.Display;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.Display;
import android.view.Surface;
import org.opencv.imgproc.Imgproc;

public class MainActivity extends Activity
        implements CvCameraViewListener {

    private CameraBridgeViewBase openCvCameraView;
    private CascadeClassifier cascadeClassifier;
    private Mat grayscaleImage;
    private int absoluteFaceSize;
    private static final String TAG = "MainActivity";
    private static final int PIXEL_WIDTH = 48;
    TensorFlowClassifier classifier;
    int pixelarray[];
    private Mat initialFrame;
    private Mat flippedFrame;
    private MatOfRect faces;
    Bitmap bm;
    Mat cropped;

    Display display;
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    initializeOpenCVDependencies();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };
    private void initializeOpenCVDependencies() {

        try {
            InputStream is = getResources().openRawResource(R.raw.haarcascade_frontalface_default);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, "haarcascade_frontalface_default.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);


            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
        } catch (Exception e) {
            Log.e("OpenCVActivity", "Error loading cascade", e);
        }

        openCvCameraView.enableView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        openCvCameraView = new JavaCameraView(this, -1);
        setContentView(openCvCameraView);
        openCvCameraView.setCvCameraViewListener(this);
    }
    private void loadModel() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    classifier=TensorFlowClassifier.create(getAssets(), "CNN",
                           "emodet.pb", "labels.txt", PIXEL_WIDTH,
                           "conv2d_input", "dense_2/Softmax", true, 7);
                } catch (final Exception e) {
                    throw new RuntimeException("Error initializing classifiers!", e);
                }
            }
        }).start();
    }
    @Override
    public void onCameraViewStarted(int width, int height) {
        grayscaleImage = new Mat(height, width, CvType.CV_8UC4);

        absoluteFaceSize = (int) (height * 0.2);
    }

    @Override
    public void onCameraViewStopped() {
    }

    private void ExportMatToBitmap(Mat img_mat, String name) {

        Bitmap bmp = Bitmap.createBitmap(img_mat.cols(), img_mat.rows(),
                Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(img_mat, bmp);
        try {
            FileOutputStream out = new FileOutputStream(name);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }


    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth, int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    private String detectEmotion(Bitmap face){
        Bitmap grayImage = toGrayscale(face);
        Bitmap resizedImage=getResizedBitmap(grayImage,48,48);

        pixelarray = new int[resizedImage.getWidth()*resizedImage.getHeight()];

        resizedImage.getPixels(pixelarray, 0, resizedImage.getWidth(), 0, 0, resizedImage.getWidth(), resizedImage.getHeight());


        float normalized_pixels [] = new float[pixelarray.length];
        for (int i=0; i < pixelarray.length; i++) {
            int pix = pixelarray[i];
            int b = pix & 0xff;
            normalized_pixels[i] = (float)(b);

        }
        System.out.println(normalized_pixels);
        Log.d("pixel_values",String.valueOf(normalized_pixels));
        String text=null;

        try{
            final Classification res = classifier.recognize(normalized_pixels);
            if (res.getLabel() == null) {
                text = " ";
            } else {
                text = String.format("%s - %.02f", res.getLabel(),res.getConf()*100);
            }}
        catch (Exception  e){
            System.out.print("Exception:"+e.toString());

        }
        return text;
    }
    @Override
    public Mat onCameraFrame(Mat aInputFrame) {
        display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = display.getRotation();

        if (orientation == Surface.ROTATION_0)
        {
            initialFrame = aInputFrame;
            flippedFrame = initialFrame.t();
            Core.flip(initialFrame.t(), flippedFrame, 1);
            Imgproc.resize(flippedFrame, flippedFrame, initialFrame.size());

            Imgproc.cvtColor(flippedFrame, grayscaleImage, Imgproc.COLOR_RGBA2RGB);

           faces = new MatOfRect();

            if (cascadeClassifier != null) {
                cascadeClassifier.detectMultiScale(grayscaleImage, faces, 1.3, 5, 2,
                        new Size(absoluteFaceSize, absoluteFaceSize), new Size());
            }
            Rect[] facesArray = faces.toArray();

            for (int i = 0; i <facesArray.length; i++) {
                cropped = flippedFrame.submat(facesArray[i]);
                bm = Bitmap.createBitmap(cropped.cols(), cropped.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(cropped, bm);
                String prediction = detectEmotion(bm);
                if (prediction == null){
                    prediction = "";
                }
                Imgproc.rectangle(flippedFrame, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0, 255), 3);
                Imgproc.putText(flippedFrame, prediction, new org.opencv.core.Point(facesArray[i].tl().x, facesArray[i].tl().y-10), Core.FONT_HERSHEY_SIMPLEX, 1.1f, new Scalar(0, 255, 0, 255), 2);
            }
            return flippedFrame;
        }
        return aInputFrame;
    }

    @Override
    public void onResume() {
        super.onResume();
            if (OpenCVLoader.initDebug()){
            Log.d(TAG, "opencv loaded");
            mLoaderCallback.onManagerConnected((LoaderCallbackInterface.SUCCESS));
            loadModel();
        }
        else{
            Log.d(TAG, "opencv not loaded");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback);
        }
    }
}