import tkinter as tk
import tkinter.ttk as ttk
from tkinter import font, filedialog
from tkinter import PhotoImage
from keras.models import load_model
import tensorflow as tf
from keras.preprocessing.image import img_to_array
import PIL.Image
from PIL import ImageTk
import numpy as np
import cv2
import json
import utils.imutils
import threading
import imageio
from keras.models import Model
import matplotlib.pyplot as plt
import matplotlib.backends.tkagg as tkagg
from matplotlib.backends.backend_agg import FigureCanvasAgg


e = threading.Event()
p = None
frame_image = None
predictions = None
filters_mainpanel = None
imagepath = None

def rgb2hex(r,g,b):
    hex = "#{:02x}{:02x}{:02x}".format(r,g,b)
    return hex
    
with open('json/data.json') as f:
    data = json.load(f)

#labels = data['labels']
#color_palette = ['#B175FF', '#AB0552', '#00C4CC', '#D1A827', '#00281F', '#D73A31', '#0B6623']
#colors = dict(zip(labels, color_palette))
#labels = data['labels']

labels = ["Furie", "Dezgust", "Frica", "Fericit", "Trist", "Surprins", "Neutru"]
colors = dict(zip(labels, [utils.imutils.random_color() for _ in range(7)]))


labelsfont = cv2.FONT_HERSHEY_SIMPLEX

model = load_model(data['model'])
model._make_predict_function()
graph =  tf.get_default_graph()
detector = cv2.CascadeClassifier(data['cascade'])
from keras import backend as K

def show_filter(layer_name):
    global imagepath, filters_mainpanel
    image = cv2.imread(imagepath)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image_roi = image.astype('float')/255.0
    image_roi = img_to_array(image_roi)
    image_roi = np.expand_dims(image_roi, axis=0)
    #print(dict([(layer.name, layer) for layer in model.layers[0]]))
    layer_dict = dict([(layer.name, layer) for layer in model.layers[0:]])
    layer_model = Model(inputs=model.input, outputs=layer_dict[layer_name].output)
    print(layer_model)
    intermediate_output = layer_model.predict(image_roi)
    #view filters
    #W = layer_model.layers[1].get_weights()
    #W = W[0]
    #w = W[:, :, :, 0]
    #print(w)
    #w = np.squeeze(w)
    #cv2.imwrite('x.png', w)
    #print(w.shape)
    x_max = intermediate_output.shape[1]
    y_max = intermediate_output.shape[2]
    n     = intermediate_output.shape[3]

    L = []
    for i in range(n):
        L.append(np.zeros((x_max, y_max)))

    for i in range(n):
        for x in range(x_max):
            for y in range(y_max):
                L[i][x][y]= intermediate_output[0][x][y][i]

    width = 200
    height = 100
    i = 0
    max_i = len(L)
    
    fig=plt.figure()
    filter_size = L[i].shape[1]
    plt.title(layer_name+' layer - '+str(max_i)+' filters : '+str(filter_size)+'x'+str(filter_size)+' size')
    plt.axis('off')  
    while i < max_i:
        columns = int(width/filter_size)
        rows = int(round(max_i/columns))
            
        
        fig.add_subplot(rows, columns, i+1)
        plt.imshow(L[i], cmap='gray', interpolation='nearest')
        plt.axis('off')
        i += 1
    plt.savefig('filters/'+layer_name+'-'+str(filter_size)+'x'+str(filter_size))
    plt.close('all')
    fig_path = 'filters/'+layer_name+'-'+str(filter_size)+'x'+str(filter_size)+'.png'
    image = cv2.imread(fig_path)
    image = utils.imutils.resize(image, new_width=665)
    #read image with OpenCV(BGR) and convert to PIL(RGB) format
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = PIL.Image.fromarray(image)
    #convert to ImageTk format
    image = ImageTk.PhotoImage(image)
    
    filters_mainpanel.configure(image=image)
    filters_mainpanel.image = image

def show_layers(image_path):
    global filters_mainpanel, imagepath
    imagepath = image_path
    filters_window = tk.Toplevel(root)
    filters_window.geometry('900x650+250+20')
    filters_window.minsize(850,665)
    filters_window.maxsize(1000,665)
    filters_window.title('Filters')
    image = PIL.Image.fromarray(np.ones((665,665,3), np.uint8)*(255))
    image = PIL.ImageTk.PhotoImage(image)
    
    filters_mainpanel = tk.Label(filters_window, image=image)
    filters_mainpanel.image = image
    filters_mainpanel.pack(side=tk.LEFT, fill=tk.BOTH)

    buttonspanel = tk.Frame(filters_window, bg='#FFFFFF')
    buttonspanel.pack(side=tk.RIGHT, fill=tk.BOTH, expand=1)

    layer_dict = dict([(layer.name, layer) for layer in model.layers[0:]])
    for layer_name, layer in layer_dict.items():
        if 'dropout' in layer_name:
            continue
        if 'normalization' in layer_name:
            continue
        if 'flatten_1' in layer_name:
            break     
        layerbtn = tk.Button(buttonspanel, text=layer_name, command=lambda layer_name=layer_name: show_filter(layer_name), font=helvet, fg='white' , bg='#01579B', height=1, activebackground ='white', activeforeground ='black', relief="groove", bd=2)
        layerbtn.pack(side=tk.TOP, fill=tk.X)    
        
    #quitbtn = tk.Button(buttonspanel, text='Quit', command=filters_window.destroy, font=helvet, fg='white' , bg='#1974D2', height=1, activebackground ='white', activeforeground ='black', relief="groove", bd=2)
    #quitbtn.pack(side=tk.TOP, fill=tk.X)
    
    
def predict(image, emotions_pannel=True):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    rects = detector.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5, flags=cv2.CASCADE_SCALE_IMAGE)
    preds = []
    tabs = len(rects)+1
    faces_predictions = []
    facenumber = 0
    colors_pred = []
    for (fX, fY, fW, fH) in rects:
        facenumber += 1
        roi = gray[fY:fY + fH, fX:fX+fW]
        roi = cv2.resize(roi, (48,48))
        cv2.imwrite('jpg/'+str(facenumber)+'.jpg', roi)
        roi = roi.astype('float')/255.0
        roi = img_to_array(roi)
        roi = np.expand_dims(roi, axis=0)
        
        with graph.as_default():
            preds  = model.predict(roi)
        predictions = dict(zip(labels, preds[0]))
        label = max(predictions, key=predictions.get)
        value = str(round(float(predictions[label])*100,2))
        if emotions_pannel or (e.is_set() and frame_image):
            faces_predictions.append(predictions)
            colors_pred.append(colors[label])
            
        if emotions_pannel or (e.is_set() and frame_image):    
            screen_text = str(facenumber)+' - ' +label+' '+ value+'%'
        else:
            screen_text = label+' '+ value+'%'
        cv2.putText(image, screen_text, (fX, fY -10), labelsfont, 0.7, colors[label], 2)
        cv2.rectangle(image, (fX, fY), (fX+fW, fY+fH), colors[label], 2, cv2.LINE_AA)
    if emotions_pannel or (e.is_set() and frame_image):
        add_emotions_percentage(faces_predictions, tabs, colors_pred)
        
    return image
    
def load_image():
    global mainpanel, frame_image
    frame_image = None
    path = filedialog.askopenfilename()
        
    if len(path) > 0:
        image = cv2.imread(path)
        image = utils.imutils.resize(image, new_width=640, new_height=480)
        image = predict(image, emotions_pannel=True)
        
        #read image with OpenCV(BGR) and convert to PIL(RGB) format
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = PIL.Image.fromarray(image)
        #convert to ImageTk format
        image = ImageTk.PhotoImage(image)
        
        mainpanel.configure(image=image)
        mainpanel.image = image
        
def load_video():
    path = filedialog.askopenfilename()
    is_video = True
    if len(path) > 0:
        cap = cv2.VideoCapture(path)
        start_recording_proc(cap, is_video)
    
def video_stream():
    cap = cv2.VideoCapture(0)
    is_video=False
    start_recording_proc(cap, is_video)
        
def startrecording(e, cap, is_video):
    global mainpanel
    cap = cap
    if is_video:
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter('video/output.avi',fourcc,  20.0, (640,480))

    while(cap.isOpened()):
        if not e.is_set():                
            ret, frame = cap.read()
            if ret==True:
                frame = cv2.flip(frame, 1)
                frame = utils.imutils.resize(frame, new_width=640)
                image = predict(frame, emotions_pannel=False)
                image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                image = PIL.Image.fromarray(image)
                global frame_image
                frame_image = image
                image = ImageTk.PhotoImage(image)
                
                mainpanel.configure(image=image)
                mainpanel.image = image
                if is_video:
                    out.write(frame)
            else:
                break
        else:
            cap.release()
            if is_video:
                out.release()
            e.clear()

def start_recording_proc(cap, is_video=False):
    global p, e
    p = threading.Thread(target=startrecording, args=(e, cap, is_video))
    p.start()
    
def snapshot():
    global frame_image, e
    if not e.is_set() and frame_image:
        filepath = filedialog.asksaveasfilename(initialdir = 'C:/Users/ele/Desktop/',title = 'Select file',filetypes = (('jpeg files','*.jpg'),('png files','*.png')), defaultextension='.jpg' )
        if not filepath:
            filepath = 'image.jpg'
        image = cv2.cvtColor(np.array(frame_image), cv2.COLOR_RGB2BGR)
        cv2.imwrite(filepath,image)

def stoprecording():
    global mainpanel, e, frame_image
    e.set()
          
def add_emotions_percentage(faces_predictions=[], faces=1, colors_pred=[]):
    notebookpanel = ttk.Notebook(emotionspanel, name='notebookpanel')
    notebookpanel.pack(side=tk.TOP, fill=tk.BOTH)
    white='#FFFFFF'
    for face in range(faces):
        if face==0:
            tab_name = 'EMOȚII'
            window_name = 'emotions'
            notebook_frame_name = tk.Frame(notebookpanel, name=window_name, bg='#FFFFFF')
            notebookpanel.add(notebook_frame_name, text=tab_name)
        else:
            tab_name ='FAȚĂ ' + str(face)
            window_name='face' + str(face)
            image_path = 'jpg/'+str(face)+'.jpg'
            image_jpg = imageio.imread('jpg/'+str(face)+'.jpg')
            imageio.imwrite('gif/'+str(face)+'.gif', image_jpg)
            
            image_name = 'gif/'+str(face)+'.gif'
            notebook_frame_name = 'notebook_frame_'+window_name
            
            notebook_frame_name = tk.Frame(notebookpanel, name=window_name)
            
            prof_img = PhotoImage(file=image_name, name=window_name)
            label = tk.Label(image=prof_img)
            label.image = prof_img # keep a reference!
            notebookpanel.add(notebook_frame_name, text=tab_name, image=prof_img, compound=tk.TOP)
            
            filtersbtn= tk.Button(notebook_frame_name, text='FILTRE', command=lambda image_path=image_path: show_layers(image_path), font=helvet, fg='white' , bg='#01579B', height=1, activebackground ='white', activeforeground ='black', relief="groove", bd=2)
            filtersbtn.pack(side=tk.BOTTOM, fill=tk.X)
            
        for label, color in colors.items():
            b, g, r = color[:]
            hexcolor = rgb2hex(r, g, b)
            stylename = 'style'+label
            stylename = ttk.Style()
            stylename.theme_use('default')
            stylename.configure(label+'.Horizontal.TProgressbar', troughcolor='#FFFFFF', background=hexcolor)

            if len(faces_predictions)> 0 and face!=0:
                value = round(float(faces_predictions[face-1][label])*100,1)
            else:
                value = 0.0
            
            emotion_label = label.upper()+': '+str(value)+'%'    
            emotionlabel = tk.Label(notebook_frame_name, text=emotion_label, font=helvet, bg='#FFFFFF', fg=hexcolor)
            emotionlabel.pack(side=tk.TOP, fill=tk.X)
            
            percentagebar = 'percentage'+label
            percentagebar = ttk.Progressbar(notebook_frame_name, style=label+'.Horizontal.TProgressbar', orient='horizontal', length=100, mode='determinate', value=value)
            percentagebar.pack(side=tk.TOP, fill=tk.X)   
 
        emptylabel = tk.Label(notebook_frame_name, text='', font=helvet, bg='#FFFFFF', fg=hexcolor)
        emptylabel.pack(side=tk.TOP, fill=tk.X)
    ttk.Style().configure("TNotebook", background=white)
    ttk.Style().map("TNotebook.Tab", background=[("selected", white)])
    ttk.Style().configure("TNotebook.Tab", background=white)
    
    
if __name__ == '__main__':
    root = tk.Tk()
    root.title('EmoDet')
    root.geometry('850x550+250+35')
    root.minsize(850,550)
    root.maxsize(1200,550)
    text = tk.Text(root)
    helvet = font.Font(family='Helvetica', size=12, weight='bold')
    text.configure(font=helvet)

    image = PIL.Image.fromarray(np.ones((480,640,3), np.uint8)*(255))
    image = PIL.ImageTk.PhotoImage(image)
    
    mainpanelroot = tk.Frame(root, bg='#FFFFFF')
    mainpanelroot.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
    
    mainpanel = tk.Label(mainpanelroot, image=image, bg='#FFFFFF')
    mainpanel.image = image
    mainpanel.pack(side=tk.LEFT, fill=tk.BOTH)
    
    emotionspanel = tk.Frame(mainpanelroot, bg='#FFFFFF')
    emotionspanel.pack(side=tk.RIGHT, fill=tk.BOTH, expand=1)
    
    buttonspanel = tk.Frame(root, bg='#FFFFFF')
    buttonspanel.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=1)

    
    streambtn = tk.Button(buttonspanel, text='Start Video', command=video_stream, font=helvet, fg='white' , bg='#01579B', height=2, width=15, activebackground ='white', activeforeground ='black', relief="groove", bd=2)
    streambtn.pack(side=tk.LEFT, fill=tk.X)
    
    snapshotbtn = tk.Button(buttonspanel, text='Poză', command = snapshot, font=helvet, fg='white' , bg='#01579B', height=2,  width=15, activebackground ='white', activeforeground ='black', relief="groove", bd=2)
    snapshotbtn.pack(side=tk.LEFT, fill=tk.X)

    stopbtn = tk.Button(buttonspanel, text='Stop Video', command=stoprecording, font=helvet, fg='white' , bg='#01579B', height=2,  width=15, activebackground ='white', activeforeground ='black', relief="groove", bd=2)
    stopbtn.pack(side=tk.LEFT, fill=tk.X)

    videobtn = tk.Button(buttonspanel, text='Video', command=load_video, font=helvet, fg='white' , bg='#01579B', height=2, width=15, activebackground ='white', activeforeground ='black', relief="groove", bd=2)
    videobtn.pack(side=tk.LEFT, fill=tk.X)
    
    add_emotions_percentage()
        
    imagebtn = tk.Button(buttonspanel, text='Imagine', command=load_image, font=helvet, fg='white' , bg='#01579B', height=2, width=25, activebackground ='white', activeforeground ='black', relief="groove", bd=2)
    imagebtn.pack(side=tk.RIGHT, fill=tk.X)


    root.mainloop()