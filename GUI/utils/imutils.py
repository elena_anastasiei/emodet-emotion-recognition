import numpy as np
import cv2

def translate(image, shift_x, shift_y):
    (height, width) = image.shape[:2]
    
    translation_matrix =  np.float32([[1, 0, shift_x], [0, 1, shift_y]])
    shifted_image = cv2.warpAffine(image, translation_matrix, (width, height))
    
    return shifted_image
    
def rotate(image, angle, rotation_point=None, scale=1.0):
    (height, width) = image.shape[:2]
    
    if rotation_point is None:
        rotation_point = (width // 2, height // 2)
        
    rotation_matrix = cv2.getRotationMatrix2D(rotation_point, angle, scale)
    rotated_image = cv2.warpAffine(image, rotation_matrix, (width, height))
    
    return rotated_image
    
def resize(image, new_width=None, new_height=None, inter=cv2.INTER_AREA):
    (height, width) = image.shape[:2]
    dim = None
    
    if new_width is None and new_height is None:
        return image
    
    if new_width is None:
        aspect_ratio = new_height / float(height)
        dim = (int(width * aspect_ratio), new_height)
    else:
        aspect_ratio = new_width / float(width)
        dim = (new_width, int(height * aspect_ratio))
    if new_height and new_height:
        if height > width:
            aspect_ratio = new_height / float(height)
            dim = (int(width * aspect_ratio), new_height)
        else:    
            aspect_ratio = new_width / float(width)
            dim = (new_width, int(height * aspect_ratio))
       
        
    resized_image = cv2.resize(image, dim, interpolation=inter)    
           
    return resized_image
    
def random_color():
    return np.random.randint(0, high=256, size=(3,)).tolist()
        