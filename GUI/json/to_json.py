import json

data = {
'model':'model/emodet.hdf5',
'cascade':'cascade/haarcascade_frontalface_default.xml',
'caffe':'caffee/res10_300x300_ssd_iter_140000.caffemodel',
'prototext':'caffee/deploy.prototxt.txt',
'labels':['Furie', 'Dezgust', 'Frica', 'Fericit', 'Trist', 'Surprins', 'Neutru']
}

with open('data.json', 'w') as f:
    json.dump(data, f)