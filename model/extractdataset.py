from scipy.misc import imsave
import numpy as np
import os

dataset_path = 'dataset/faces/'

##############################################################################
#filepath = 'dataset/fer2013.csv'
#classes = ['Anger', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']
#for foldername in classes:
#    if not os.path.exists(dataset_path+foldername):
#        os.makedirs(dataset_path+foldername)
# 
#def image_from_array(image):
#    image = np.asarray(image, dtype=np.uint8).reshape(48,48)
#    return image
##############################################################################

def preocess_dataset(filepath):
    images = []
    labels = []
    i = 0
        
    with open(filepath) as f:
        next(f)
        for line in f:
            
            data = line.split(',')
            
            #for each line process pixels list as array and append to images
            image = [int(pixel) for pixel in data[1].split()]
            images.append(image)
            labels.append(data[0])
            
            ######################################################################################
            #Save images from csv raw lines using  and
            #label = classes[int(data[0])]
            #imsave(dataset_path+label+'/{}.png'.format(str(i).zfill(6)), image_from_array(image))
            ######################################################################################
            i+=1
            #28709
            if i == 28709:
                break;
            
                
    images, labels = np.array(images)/255.0, np.array(labels)
    
    #images.shape = (35887 images, 2304 size of each image)
    n_of_image, size_of_image = images.shape
    
    #images = images(35887, width=48, height=48, depth=1)
    images = images.reshape(n_of_image, 48, 48, 1)
    print('Ammount of images in dataset', n_of_image)
    return images, labels