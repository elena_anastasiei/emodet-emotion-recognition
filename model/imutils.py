import cv2
import numpy as np

def resize(image, w=None, h=None, inter=cv2.INTER_AREA):
    dim = None
    (height, width) = image.shape[:2]
    
    if w is None and h is None:
        return image
        
    if w is None:
        r = h / float(height)
        dim = (int(width * r), h)
    else:
        r = w / float(width)
        dim = (w, int(height*r))
   
    resized = cv2.resize(image, dim, interpolation=inter)
    
    return resized
    
def random_color():
    return np.random.randint(0, high=256, size=(3,)).tolist()    
                    