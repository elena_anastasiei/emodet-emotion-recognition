_________________________________________________________________
Layer (type)                 Output Shape              Param #
=================================================================
conv2d_1 (Conv2D)            (None, 48, 48, 16)        416
_________________________________________________________________
activation_1 (Activation)    (None, 48, 48, 16)        0
_________________________________________________________________
batch_normalization_1 (Batch (None, 48, 48, 16)        64
_________________________________________________________________
conv2d_2 (Conv2D)            (None, 48, 48, 16)        6416
_________________________________________________________________
activation_2 (Activation)    (None, 48, 48, 16)        0
_________________________________________________________________
batch_normalization_2 (Batch (None, 48, 48, 16)        64
_________________________________________________________________
max_pooling2d_1 (MaxPooling2 (None, 24, 24, 16)        0
_________________________________________________________________
dropout_1 (Dropout)          (None, 24, 24, 16)        0
_________________________________________________________________
conv2d_3 (Conv2D)            (None, 24, 24, 32)        4640
_________________________________________________________________
activation_3 (Activation)    (None, 24, 24, 32)        0
_________________________________________________________________
batch_normalization_3 (Batch (None, 24, 24, 32)        128
_________________________________________________________________
conv2d_4 (Conv2D)            (None, 24, 24, 32)        9248
_________________________________________________________________
activation_4 (Activation)    (None, 24, 24, 32)        0
_________________________________________________________________
batch_normalization_4 (Batch (None, 24, 24, 32)        128
_________________________________________________________________
max_pooling2d_2 (MaxPooling2 (None, 12, 12, 32)        0
_________________________________________________________________
dropout_2 (Dropout)          (None, 12, 12, 32)        0
_________________________________________________________________
conv2d_5 (Conv2D)            (None, 12, 12, 64)        18496
_________________________________________________________________
activation_5 (Activation)    (None, 12, 12, 64)        0
_________________________________________________________________
batch_normalization_5 (Batch (None, 12, 12, 64)        256
_________________________________________________________________
conv2d_6 (Conv2D)            (None, 12, 12, 64)        36928
_________________________________________________________________
activation_6 (Activation)    (None, 12, 12, 64)        0
_________________________________________________________________
batch_normalization_6 (Batch (None, 12, 12, 64)        256
_________________________________________________________________
max_pooling2d_3 (MaxPooling2 (None, 6, 6, 64)          0
_________________________________________________________________
dropout_3 (Dropout)          (None, 6, 6, 64)          0
_________________________________________________________________
flatten_1 (Flatten)          (None, 2304)              0
_________________________________________________________________
dense_1 (Dense)              (None, 512)               1180160
_________________________________________________________________
activation_7 (Activation)    (None, 512)               0
_________________________________________________________________
batch_normalization_7 (Batch (None, 512)               2048
_________________________________________________________________
dropout_4 (Dropout)          (None, 512)               0
_________________________________________________________________
dense_2 (Dense)              (None, 7)                 3591
_________________________________________________________________
activation_8 (Activation)    (None, 7)                 0
=================================================================
Total params: 1,262,839
Trainable params: 1,261,367
Non-trainable params: 1,472
_________________________________________________________________
Train on 26915 samples, validate on 8972 samples
Epoch 1/50
26915/26915 [==============================] - 823s 31ms/step - loss: 2.1144 - acc: 0.2772 - val_loss: 1.7699 - val_acc: 0.3095

Epoch 00001: val_loss improved from inf to 1.76993, saving model to model/emodet.hdf5
Epoch 2/50
26915/26915 [==============================] - 792s 29ms/step - loss: 1.8267 - acc: 0.3380 - val_loss: 1.5819 - val_acc: 0.3914

Epoch 00002: val_loss improved from 1.76993 to 1.58185, saving model to model/emodet.hdf5
Epoch 3/50
26915/26915 [==============================] - 774s 29ms/step - loss: 1.6581 - acc: 0.3856 - val_loss: 1.5038 - val_acc: 0.4245

Epoch 00003: val_loss improved from 1.58185 to 1.50382, saving model to model/emodet.hdf5
Epoch 4/50
26915/26915 [==============================] - 821s 30ms/step - loss: 1.5581 - acc: 0.4160 - val_loss: 1.4552 - val_acc: 0.4391

Epoch 00004: val_loss improved from 1.50382 to 1.45520, saving model to model/emodet.hdf5
Epoch 5/50
26915/26915 [==============================] - 925s 34ms/step - loss: 1.4046 - acc: 0.4640 - val_loss: 1.3038 - val_acc: 0.5029

Epoch 00005: val_loss improved from 1.45520 to 1.30375, saving model to model/emodet.hdf5
Epoch 6/50
26915/26915 [==============================] - 871s 32ms/step - loss: 1.3490 - acc: 0.4818 - val_loss: 1.2900 - val_acc: 0.5050

Epoch 00006: val_loss improved from 1.30375 to 1.29004, saving model to model/emodet.hdf5
Epoch 7/50
26915/26915 [==============================] - 894s 33ms/step - loss: 1.3083 - acc: 0.5002 - val_loss: 1.2555 - val_acc: 0.5217

Epoch 00007: val_loss improved from 1.29004 to 1.25553, saving model to model/emodet.hdf5
Epoch 8/50
26915/26915 [==============================] - 920s 34ms/step - loss: 1.2832 - acc: 0.5083 - val_loss: 1.2422 - val_acc: 0.5266

Epoch 00008: val_loss improved from 1.25553 to 1.24216, saving model to model/emodet.hdf5
Epoch 9/50
26915/26915 [==============================] - 907s 34ms/step - loss: 1.2591 - acc: 0.5203 - val_loss: 1.2157 - val_acc: 0.5354

Epoch 00009: val_loss improved from 1.24216 to 1.21572, saving model to model/emodet.hdf5
Epoch 10/50
26915/26915 [==============================] - 899s 33ms/step - loss: 1.2018 - acc: 0.5406 - val_loss: 1.1819 - val_acc: 0.5492

Epoch 00010: val_loss improved from 1.21572 to 1.18190, saving model to model/emodet.hdf5
Epoch 11/50
26915/26915 [==============================] - 934s 35ms/step - loss: 1.1723 - acc: 0.5548 - val_loss: 1.1829 - val_acc: 0.5534

Epoch 00011: val_loss did not improve from 1.18190
Epoch 12/50
26915/26915 [==============================] - 923s 34ms/step - loss: 1.1537 - acc: 0.5594 - val_loss: 1.1746 - val_acc: 0.5539

Epoch 00012: val_loss improved from 1.18190 to 1.17456, saving model to model/emodet.hdf5
Epoch 13/50
26915/26915 [==============================] - 778s 29ms/step - loss: 1.1418 - acc: 0.5681 - val_loss: 1.1657 - val_acc: 0.5648

Epoch 00013: val_loss improved from 1.17456 to 1.16569, saving model to model/emodet.hdf5
Epoch 14/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.1280 - acc: 0.5709 - val_loss: 1.1579 - val_acc: 0.5641

Epoch 00014: val_loss improved from 1.16569 to 1.15792, saving model to model/emodet.hdf5
Epoch 15/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.1024 - acc: 0.5809 - val_loss: 1.1541 - val_acc: 0.5663

Epoch 00015: val_loss improved from 1.15792 to 1.15415, saving model to model/emodet.hdf5
Epoch 16/50
26915/26915 [==============================] - 776s 29ms/step - loss: 1.0829 - acc: 0.5891 - val_loss: 1.1424 - val_acc: 0.5760

Epoch 00016: val_loss improved from 1.15415 to 1.14235, saving model to model/emodet.hdf5
Epoch 17/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0800 - acc: 0.5876 - val_loss: 1.1392 - val_acc: 0.5759

Epoch 00017: val_loss improved from 1.14235 to 1.13922, saving model to model/emodet.hdf5
Epoch 18/50
26915/26915 [==============================] - 780s 29ms/step - loss: 1.0717 - acc: 0.5941 - val_loss: 1.1323 - val_acc: 0.5798

Epoch 00018: val_loss improved from 1.13922 to 1.13232, saving model to model/emodet.hdf5
Epoch 19/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0579 - acc: 0.6004 - val_loss: 1.1334 - val_acc: 0.5761

Epoch 00019: val_loss did not improve from 1.13232
Epoch 20/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0436 - acc: 0.6085 - val_loss: 1.1331 - val_acc: 0.5782

Epoch 00020: val_loss did not improve from 1.13232
Epoch 21/50
26915/26915 [==============================] - 776s 29ms/step - loss: 1.0449 - acc: 0.6033 - val_loss: 1.1298 - val_acc: 0.5784

Epoch 00021: val_loss improved from 1.13232 to 1.12980, saving model to model/emodet.hdf5
Epoch 22/50
26915/26915 [==============================] - 780s 29ms/step - loss: 1.0297 - acc: 0.6112 - val_loss: 1.1221 - val_acc: 0.5847

Epoch 00022: val_loss improved from 1.12980 to 1.12214, saving model to model/emodet.hdf5
Epoch 23/50
26915/26915 [==============================] - 777s 29ms/step - loss: 1.0299 - acc: 0.6103 - val_loss: 1.1201 - val_acc: 0.5852

Epoch 00023: val_loss improved from 1.12214 to 1.12014, saving model to model/emodet.hdf5
Epoch 24/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0304 - acc: 0.6106 - val_loss: 1.1294 - val_acc: 0.5799

Epoch 00024: val_loss did not improve from 1.12014
Epoch 25/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0171 - acc: 0.6150 - val_loss: 1.1173 - val_acc: 0.5842

Epoch 00025: val_loss improved from 1.12014 to 1.11731, saving model to model/emodet.hdf5
Epoch 26/50
26915/26915 [==============================] - 776s 29ms/step - loss: 1.0190 - acc: 0.6145 - val_loss: 1.1156 - val_acc: 0.5877

Epoch 00026: val_loss improved from 1.11731 to 1.11559, saving model to model/emodet.hdf5
Epoch 27/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0140 - acc: 0.6151 - val_loss: 1.1164 - val_acc: 0.5870

Epoch 00027: val_loss did not improve from 1.11559
Epoch 28/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0055 - acc: 0.6218 - val_loss: 1.1213 - val_acc: 0.5831

Epoch 00028: val_loss did not improve from 1.11559
Epoch 29/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0134 - acc: 0.6178 - val_loss: 1.1171 - val_acc: 0.5850

Epoch 00029: val_loss did not improve from 1.11559
Epoch 30/50
26915/26915 [==============================] - 779s 29ms/step - loss: 1.0076 - acc: 0.6200 - val_loss: 1.1150 - val_acc: 0.5864

Epoch 00030: val_loss improved from 1.11559 to 1.11505, saving model to model/emodet.hdf5
Epoch 31/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0035 - acc: 0.6207 - val_loss: 1.1136 - val_acc: 0.5883

Epoch 00031: val_loss improved from 1.11505 to 1.11356, saving model to model/emodet.hdf5
Epoch 32/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0052 - acc: 0.6204 - val_loss: 1.1136 - val_acc: 0.5894

Epoch 00032: val_loss did not improve from 1.11356
Epoch 33/50
26915/26915 [==============================] - 775s 29ms/step - loss: 1.0012 - acc: 0.6249 - val_loss: 1.1123 - val_acc: 0.5884

Epoch 00033: val_loss improved from 1.11356 to 1.11229, saving model to model/emodet.hdf5
Epoch 34/50
26915/26915 [==============================] - 775s 29ms/step - loss: 0.9953 - acc: 0.6234 - val_loss: 1.1136 - val_acc: 0.5889

Epoch 00034: val_loss did not improve from 1.11229
Epoch 35/50
26915/26915 [==============================] - 774s 29ms/step - loss: 0.9994 - acc: 0.6236 - val_loss: 1.1135 - val_acc: 0.5896

Epoch 00035: val_loss did not improve from 1.11229
Epoch 36/50
26915/26915 [==============================] - 775s 29ms/step - loss: 0.9961 - acc: 0.6226 - val_loss: 1.1137 - val_acc: 0.5885

Epoch 00036: val_loss did not improve from 1.11229
Epoch 37/50
26915/26915 [==============================] - 774s 29ms/step - loss: 0.9948 - acc: 0.6271 - val_loss: 1.1124 - val_acc: 0.5874

Epoch 00037: val_loss did not improve from 1.11229
Epoch 38/50
26915/26915 [==============================] - 774s 29ms/step - loss: 0.9932 - acc: 0.6249 - val_loss: 1.1117 - val_acc: 0.5891

Epoch 00038: val_loss improved from 1.11229 to 1.11175, saving model to model/emodet.hdf5
Epoch 39/50
26915/26915 [==============================] - 774s 29ms/step - loss: 0.9893 - acc: 0.6297 - val_loss: 1.1129 - val_acc: 0.5884

Epoch 00039: val_loss did not improve from 1.11175
Epoch 40/50
26915/26915 [==============================] - 775s 29ms/step - loss: 0.9883 - acc: 0.6300 - val_loss: 1.1119 - val_acc: 0.5888

Epoch 00040: val_loss did not improve from 1.11175
Epoch 41/50
26915/26915 [==============================] - 775s 29ms/step - loss: 0.9928 - acc: 0.6249 - val_loss: 1.1124 - val_acc: 0.5881

Epoch 00041: val_loss did not improve from 1.11175
Epoch 42/50
26915/26915 [==============================] - 774s 29ms/step - loss: 0.9926 - acc: 0.6249 - val_loss: 1.1122 - val_acc: 0.5879

Epoch 00042: val_loss did not improve from 1.11175
Epoch 43/50
26915/26915 [==============================] - 775s 29ms/step - loss: 0.9873 - acc: 0.6282 - val_loss: 1.1128 - val_acc: 0.5882

Epoch 00043: val_loss did not improve from 1.11175
Epoch 44/50
26915/26915 [==============================] - 774s 29ms/step - loss: 0.9904 - acc: 0.6279 - val_loss: 1.1119 - val_acc: 0.5878

Epoch 00044: val_loss did not improve from 1.11175
Epoch 45/50
26915/26915 [==============================] - 774s 29ms/step - loss: 0.9905 - acc: 0.6251 - val_loss: 1.1131 - val_acc: 0.5876

Epoch 00045: val_loss did not improve from 1.11175
Epoch 46/50
26915/26915 [==============================] - 774s 29ms/step - loss: 0.9900 - acc: 0.6270 - val_loss: 1.1121 - val_acc: 0.5882

Epoch 00046: val_loss did not improve from 1.11175
Epoch 47/50
26915/26915 [==============================] - 774s 29ms/step - loss: 0.9882 - acc: 0.6283 - val_loss: 1.1122 - val_acc: 0.5881

Epoch 00047: val_loss did not improve from 1.11175
Epoch 48/50
26915/26915 [==============================] - 775s 29ms/step - loss: 0.9932 - acc: 0.6285 - val_loss: 1.1119 - val_acc: 0.5876

Epoch 00048: val_loss did not improve from 1.11175
Epoch 49/50
26915/26915 [==============================] - 775s 29ms/step - loss: 0.9890 - acc: 0.6295 - val_loss: 1.1117 - val_acc: 0.5891

Epoch 00049: val_loss improved from 1.11175 to 1.11168, saving model to model/emodet.hdf5
Epoch 50/50
26915/26915 [==============================] - 774s 29ms/step - loss: 0.9921 - acc: 0.6240 - val_loss: 1.1119 - val_acc: 0.5885

Epoch 00050: val_loss did not improve from 1.11168
             precision    recall  f1-score   support

      Anger       0.50      0.45      0.47      1238
    Disgust       0.71      0.37      0.49       137
       Fear       0.49      0.31      0.38      1280
      Happy       0.76      0.83      0.80      2247
        Sad       0.43      0.50      0.46      1519
   Surprise       0.75      0.72      0.74      1001
    Neutral       0.51      0.60      0.55      1550

avg / total       0.59      0.59      0.58      8972