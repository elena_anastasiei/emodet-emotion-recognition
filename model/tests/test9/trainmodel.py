from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from emodetnet import EmoDetNet
from keras.callbacks import ModelCheckpoint
from keras.optimizers import SGD, Adadelta, Adam
from keras.utils import np_utils
from keras.utils import plot_model
from extractdataset import preocess_dataset
from trainingmonitor import TrainingMonitor
from keras.callbacks import LearningRateScheduler
from keras.callbacks import ReduceLROnPlateau
from keras.callbacks import EarlyStopping
import plotmodel
import matplotlib.pyplot as plt
import numpy as np
import os

os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin/'


dataset_path = 'dataset/fer2013.csv'
model_path = 'model/emodet.hdf5'
plots_path = 'architecture/plots/'
logs_path = 'architecture/logs/'
#weights_path = 'architecture/weights/'
emodetnet_architecture_path= 'architecture/architecture/emodetnet.png'


epochs = 50
batch_size = 64
height, width = 48, 48
depth = 1
classes = 7
test_size = 0.25
random_state = 42
target_names = ['Anger', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']
metrics = ['accuracy']
loss = 'categorical_crossentropy'
#optimizer = Adadelta(lr=1.0, rho=0.95, epsilon=1e-08, decay=0.0)
optimizer = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-7)
#optimizer = SGD(lr=0.01, momentum=0.9, nesterov=False)

def step_decay(epoch):
    initAlpha = 0.01
    factor = 0.25
    dropEvery = 5
    alpha = initAlpha * (factor ** np.floor((1 + epoch) / dropEvery))
    return float(alpha)



#file_name = os.path.sep.join([weights_path,"weights-{epoch:03d}-{val_loss:.4f}.hdf5"])
#checkpoint = ModelCheckpoint(file_name, monitor='val_loss', mode='min', save_best_only=True, verbose=1)
lr_reducer = ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=3, verbose=1)
#early_stopper = EarlyStopping(monitor='val_loss', min_delta=0, patience=8, verbose=1, mode='auto')

checkpoint = ModelCheckpoint(model_path, monitor="val_loss", save_best_only=True, verbose=1)
figPath = os.path.sep.join([plots_path, "{}.png".format(os.getpid())])
jsonPath = os.path.sep.join([logs_path, "{}.json".format(os.getpid())])

callbacks = [lr_reducer, checkpoint]

# TrainingMonitor(figPath, jsonPath=jsonPath)
# LearningRateScheduler(step_decay)

images, labels = preocess_dataset(dataset_path)
le = LabelEncoder().fit(labels)
labels = np_utils.to_categorical(le.transform(labels), 7)
print(labels)

(trainX, testX, trainY, testY) = train_test_split(images, labels, test_size=test_size, stratify=labels, random_state=random_state)


model = EmoDetNet.build(width=width, height=height, depth=depth, classes=classes)
model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
plot_model(model, to_file=emodetnet_architecture_path, show_shapes=True)

n_classes = labels.sum(axis=0)
class_weight = n_classes.max()/n_classes
print(class_weight)
H = model.fit(trainX, trainY, validation_data=(testX, testY), callbacks=callbacks, class_weight=class_weight, batch_size=batch_size, epochs=epochs)

predictions = model.predict(testX, batch_size=batch_size)
print(classification_report(testY.argmax(axis=1), predictions.argmax(axis=1), target_names=target_names))

model.save(model_path)

try:
    plotmodel.model_plot(epochs, H, 'final.png')
except:
    print('error plotting')
