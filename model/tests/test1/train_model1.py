from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from emodetnet import EmoDetNet
from keras.callbacks import ModelCheckpoint
from keras.optimizers import SGD
from keras.utils import np_utils
from keras.utils import plot_model
from extract_dataset import preocess_dataset
from trainingmonitor import TrainingMonitor
from keras.callbacks import LearningRateScheduler
import numpy as np
import os

os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin/'

#Folder names to the dataset, model, net arhitecure, plots, logs
dataset_path = 'dataset/fer2013.csv'
model_path = 'model/emodet.hdf5'
plot_folder = 'architecture/plots/'
logs_folder = 'architecture/logs/'
weights_folder = 'architecture/weights/'
emodetnet_architecture_folder= 'architecture/architecture/'
labelNames = ['Anger', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']

#Inintialize the parameters
epochs = 25
batch_size = 64
height, width = 48, 48
depth = 1
classes = 7
test_size = 0.25
random_state = 42
metrics = ['accuracy']
loss = 'categorical_crossentropy'



def step_decay(epoch):
    # initialize the base initial learning rate, drop factor, and
    #epochs to drop every
    initAlpha = 0.01
    factor = 0.25
    dropEvery = 5
    # compute learning rate for the current epoch
    alpha = initAlpha * (factor ** np.floor((1 + epoch) / dropEvery))
    # return the learning rate
    return float(alpha)

#Construct the callback to save only the *best* model to disk
# based on the validation loss

#file_name = os.path.sep.join([weights_folder,"weights-{epoch:03d}-{val_loss:.4f}.hdf5"])
#checkpoint = ModelCheckpoint(file_name, monitor='val_loss', mode='min', save_best_only=True, verbose=1)

checkpoint = ModelCheckpoint(model_path, monitor="val_loss", save_best_only=True, verbose=1)

jsonPath = os.path.sep.join([logs_folder, "{}.json".format(os.getpid())])
figPath = os.path.sep.join([plot_folder, "{}.png".format(os.getpid())])
callbacks = [LearningRateScheduler(step_decay), checkpoint, TrainingMonitor(figPath, jsonPath=jsonPath)]


#Load images and labels form fer2013.csv files
images, labels = preocess_dataset(dataset_path)

#Encode labels, one-hot encoding, 7 classes
le = LabelEncoder().fit(labels)
labels = np_utils.to_categorical(le.transform(labels), 7)

#Handle data inbalance
n_classes = labels.sum(axis=0)
class_weight = n_classes.max()/n_classes

#Split data, training and testing
(trainX, testX, trainY, testY) = train_test_split(images, labels, test_size=test_size, stratify=labels, random_state=random_state)

#Define Gradient Descent optimizer with a learning rate of 0.01
sgd = SGD(lr=0.01, momentum=0.9, nesterov=True)

#Compiling model, width=48, height=48, depth=1 - configuring learning process
model = EmoDetNet.build(width=width, height=height, depth=depth, classes=classes)
model.compile(loss=loss, optimizer=sgd, metrics=metrics)
plot_model(model, to_file=emodetnet_architecture_folder+"emodetnet.png", show_shapes=True)


#Training network, fit model with training data, evaluate model on testing data
H = model.fit(trainX, trainY, validation_data=(testX, testY), callbacks=callbacks, class_weight=class_weight, batch_size=batch_size, epochs=epochs)

#Predict labels for testing data
predictions = model.predict(testX, batch_size=batch_size)
print(classification_report(testY.argmax(axis=1), predictions.argmax(axis=1), target_names=labelNames))

#Save model
model.save(model_path)

#Plot training+testing loss and accuracy
#plot_model(epochs, H)


